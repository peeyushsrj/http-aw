http-available-website
======================

Out of a list of websites, it checks for available websites and outputs them. Check for website in HTTPS protocol if HTTP is not available. 

### In action
![httpaw in action](screenshot.png)

### File conventions
Input File : input.txt<br>
[User Agent File](https://en.wikipedia.org/wiki/User_agent): useragent.txt (default Chrome/41.0.2228.0)<br>
Available Websites : output.txt<br>

### Instructions to Run

Unzip the [downloaded file](https://github.com/peeyushsrj/http-available-websites/archive/master.zip) and run ./httpaw

```
chmod +x httpaw
./httpaw
```

### Requirements

Linux